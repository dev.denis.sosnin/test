<?
require_once "../vendor/autoload.php";

use Symfony\Component\HttpFoundation\Request;
$request = Request::createFromGlobals();

// Debug
use Symfony\Component\Debug\Debug;
Debug::enable();