<?
use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Response;
use App\Classes\GooglePlaceAutocomplete;

$routes = new Routing\RouteCollection();

// home
$routes->add('home', new Routing\Route('/', array("_controller" => function($request){
    ob_start();
    include __DIR__.'/../back/Templates/home.php';
    return new Response(ob_get_clean());
})));

// rest api
use Symfony\Component\HttpFoundation\AcceptHeader;
$routes->add('api', new Routing\Route('/api', array('_controller' => function($request){
    
    // check
    $acceptHeader = AcceptHeader::fromString($request->headers->get('Accept'));
    if($acceptHeader->has("application/json"))
    {
        $data = GooglePlaceAutocomplete::suggest($request->query->get('query') );
        return new Response( json_encode( $data ) );
    }

    return new Response('no ajax');

})));

return $routes;