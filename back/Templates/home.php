<!DOCTYPE html>
<html lang="en">
<head>
<meta name="google-site-verification" content="zdEyNQTmmKOelSTvc5jFMKzEulurnPe6MjCbLbi0Zvc" />
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <title>Выбор адреса | Для ГрузовичкоФ</title>
</head>
<body>
    <div id="app">
        <v-search></v-search>
    </div>
    <script src="/assets/main.js"></script>
</body>
</html>