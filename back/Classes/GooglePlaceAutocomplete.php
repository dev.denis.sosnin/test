<?

namespace App\Classes;

class GooglePlaceAutocomplete
{
    public function suggest($fields)
    {
       
        $result = false;
        $params = explode(" ", $fields);

        $request = array_reduce($params, function($carry, $item){
            $carry = $carry.'+'.$item;
            return $carry;
        });

        if ($ch = curl_init("https://maps.googleapis.com/maps/api/place/autocomplete/json?input=$request&key=AIzaSyBcx_BqZyq_ew_QLBDId6qrtXMGecCoOQo&session_token=1234567890"))
        {
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            $result = curl_exec($ch);
            $result = json_decode($result, true);
            curl_close($ch);
        }
        return $result;
    }
}