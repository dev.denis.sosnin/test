<?
require_once __DIR__."/../back/bootstrap.php";
$routes = include __DIR__."/../back/routing.php";

use Symfony\Component\Routing;
use Symfony\Component\HttpFoundation\Response;

$context = new Routing\RequestContext();
$context->fromRequest($request);

$matcher = new Routing\Matcher\UrlMatcher($routes, $context);
try {
    $parameters = $matcher->match($request->getPathInfo());
    $response = call_user_func($parameters['_controller'], $request);
} catch (Routing\Exception\ResourceNotFoundException $e) {
    $response = new Response('Not Found', 404);
} catch (Exception $e) {
    $response = new Response('An error occurred', 500);
}

$response->send();