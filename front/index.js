import "./stylus/index.styl"
import Vue from "vue"
import VueRx from 'vue-rx'
import search from './components/search.vue'
import google from './plugins/google-maps'

Vue.use(google, {
    apiKey: 'AIzaSyBcx_BqZyq_ew_QLBDId6qrtXMGecCoOQo',
    libraries: ['places','drawing','visualization']
})

Vue.use(VueRx)

let app = new Vue({
    el: '#app',
    components: {
        "v-search": search
    }
})