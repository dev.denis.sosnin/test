import loader from './loader'
import map from './components/google-map.vue'

const google = {
	install (Vue, options) {
		Vue.component('google-map', map)

		loader.load(options)
		Vue.mixin({
			created () {
				this.$loader = loader
			}
		})
	}
}

export default google